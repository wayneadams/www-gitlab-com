---
layout: handbook-page-toc
title: "Partner Help Desk"
---
 
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}




# Welcome to the Partner Help Desk page
This page is intended for Partner Help Desk specialists to document and review processes and procedures, as well as anyone looking for information on what the team handles and how to contact us.

**If you are a channel or sales team member looking for easy-to-read information and links, please visit the [Partner Support page](https://about.gitlab.com/handbook/resellers/partner-support/).**


## Partner Help Desk links & documents

<details>
<summary markdown="span">Quick Links</summary>

- [PHD Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/4547475?label_name%5B%5D=Partner%20Help%20Desk)
- [Channels Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1508300?label_name[]=Channel)
- [Channel Ops Issue Board](https://gitlab.com/gitlab-com/sales-team/field-operations/channel-operations/-/boards)
- [NFR Requests](https://gitlab-com.gitlab.io/support/internal-requests-form/)
- [PHD Improvement Issue](https://gitlab.com/gitlab-com/channel/channels/-/issues/816)
- [Worldwide Tax Forms](https://gitlab.com/gitlab-com/Finance-Division/tax-team/-/issues/272)
- [Vendor Set Up Forms](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#how-to-process-customer-requested-vendor-setup-forms)

</details>

<details>
<summary markdown="span">Docs & Spreadsheets</summary>
- [Partner Support FAQ](https://docs.google.com/document/d/1rygv7btm5dl-iQrVD30zOYWq98R0dsGYNjIHTBmczGY/edit#)
- [Internal GitLab Channel Partner Program Discounts and Incentive Guide](https://docs.google.com/document/d/1qiT_2EsnL20c4w0hyZ_CGaJQIzj8CSCsHERoR80cwws/edit?usp=sharing)
- [CAM Mapping](https://docs.google.com/spreadsheets/d/1okdK1HoqM-POt6GySRadeFp5AWf0rJY43GoScUq0EX0/edit#gid=916657633)
- [Certification Dashboard](https://docs.google.com/spreadsheets/d/147DUeV4k2ybqftcnJcBR6SOoZWxGpjoCfNyn1hNVfAg/edit#gid=1799234939)
- [NFR License Request Form](https://docs.google.com/spreadsheets/d/1AXB3ERYQMQ3RmtweidgxR18rdlb8ZObqicanOPgWyVA/edit#gid=1914005562)
- [Partner Locator Leads](https://docs.google.com/spreadsheets/d/1JTb45fPz0hDzAnxsbhKwRHIX81640SFnzfVFp5HNs4U/edit#gid=0)
- [Partner Maintenance Calendar](https://docs.google.com/document/u/0/d/10PE4DnGW8oila0RN4sk4uJcD6xeoBFgGWeqFFIIhwA4/edit)
- [Partner Guide](https://docs.google.com/document/d/1HOzcdl22JRRbqo0SLPDsoUiM8NpNkf6-L2eiUHr6HZ4/edit)
- [Partner Payout Google Drive](https://drive.google.com/drive/u/0/folders/1eDTge527y4XtiOdI52rsIL2e7ItIK1vA)
- [Common Response Template](https://docs.google.com/spreadsheets/d/1iPJh3sP6p3fc_FfJPe7d5uzWThe6FJ5W_3arGFm2Dj4/edit?usp=sharing)
- [Working with Partner Help Desk](https://docs.google.com/presentation/u/0/d/1tT5xcx04mlFyuftL5ECPH1VCZ0pkhW7caqnCkM7a-Ro/edit)
- [CAM Onboarding (PHD Style)](https://docs.google.com/document/d/1izOlntB_Ie7TlZ9y6e3SHZU40d-voXR_6MhZJWmTk8g/edit)
- [Thought Industries: Reference Guide for PHD](https://docs.google.com/document/d/1Bu-xtqeWluu_Od6g7vvl67XLStPemIEh2gyLQNY5SB8/edit)
- [Bulk Updating SFDC Account Ownership (a spreadsheet for Channel Directors)](https://docs.google.com/spreadsheets/d/17knbgkzPDLUDtk9kWSHlKL6LnHngNiN5NgrSLG4Um14/edit#gid=0)
- [Channel Enablement Deck](https://docs.google.com/presentation/d/1r99KVp_26yaHJwloEawHRI5wtEJmNljcCXitmPC8Mkk/edit?usp=sharing)
</details>

<details>
<summary markdown="span">Handbook Pages</summary>
- [PHD Job Description](https://about.gitlab.com/job-families/sales/partner-help-desk-specialist/)
- [Channel Partner Handbook](https://about.gitlab.com/handbook/resellers/)
- [Channel Operations](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/)
- [Channel Programs Operations](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops)
- [Deal Desk](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#)
- [Deal Registration Program Overview](https://about.gitlab.com/handbook/resellers/channel-working-with-GitLab/#deal-registration-program-overview)
- [NFR Policy](https://about.gitlab.com/handbook/resellers/channel-working-with-GitLab/#not-for-resale-nfr-program-and-policy)
- [Partner Certifications & Training](https://about.gitlab.com/handbook/resellers/training/)
- [Channel Program Guide](https://about.gitlab.com/handbook/resellers/Channel-Program-Guide/)
</details>

<details>
<summary markdown="span">Process Docs & Training Guides</summary>
- [PHD Partner Payment Process](https://docs.google.com/document/u/0/d/17KMl0iUX96cP9uKNsJpiznME9Tf7loXPPpNbf9TAoWU/edit)
- [Impartner Certification Uploads](https://gitlab.zoom.us/rec/share/A3LGszeVNhfLKHCqvs2Q9oDA7hyuV5RJPT_LayzXpEuMiWvXEx6WXqaBBvmUvq2n.Btee--CGckwTqHQw) (pw: ^ubeHR3W)
- [Building Partner Insights](https://docs.google.com/document/d/1jFo7kQ6CZwNzRP1WXbyCUrPx16qUmPtXyAtaAyyhJIE/edit#heading=h.m3xtrm4e04xn)
</details>

<details>
<summary markdown="span">Partner Portal Asset Links (portal login required)</summary>
- [Partner Guide](https://partners.gitlab.com/prm/English/s/assets?id=404715)
- [Online Agreement](https://partners.gitlab.com/prm/English/s/assets?id=290599)
- [Deal Registration Guide](https://partners.gitlab.com/prm/English/s/assets?id=391183)
- [Partner Locator Guide](https://partners.gitlab.com/prm/English/s/assets?id=288270)
- [GitLab Program Discounts and Incentive Guide - Commercial](https://partners.gitlab.com/prm/English/s/assets?id=350001)
</details>

## Channel Manager Ownership in SFDC - Accounts and Opportunities

### Channel Manager Account Ownership

Channel Managers are managed differently depending on the region they are in. 
- US PubSec Channel Managers each have owned partner accounts. 
- All other regions of the world have Select Channel Managers assigned to specific areas, and Commercial Channel Managers (who handle Open Partners) assigned to specific areas. See below for the region breakdown. 

When there are changes to each channel team, the corresponding PHD Specialist should create an MR with the update. The Channel Marketing team updates the CAMs in Impartner.

When updating the Account Owner on a partner account, make sure to also update the `Contracted Territories` field.

#### Updating an Account Owner in SFDC (Channel Accounts ONLY)
1. Navigate to the account record in SFDC.
2. Under the `GitLab Partner Program Info`, check to make sure the “Public Sector Partner” field is set to “No.” (Unless the account is US PubSec.)
3. Click the `Change Account Owner` button at the top of the record. 
4. Type in the Channel Manager’s name, and then click “Save.” Do not change any check boxes.


#### Channel Managers by GEO
<details>
<summary markdown="span">AMER Select</summary>
- East: Maria Henry 
- West: TBD/Lisa Cartagena (acting)
- Central: Joe McAninch
- Canada: TBD/Lisa Cartagena (acting)
- LATAM: Rodrigo Rios

</details>


<details>
<summary markdown="span">AMER Open</summary>
- East: Jay Bahar
- West: Lisa Cartagena
- Central: Lisa Cartagena
- Canada: Lisa Cartagena
- LATAM: Rodrigo Rios 

</details>


<details>
<summary markdown="span">APAC</summary>
- ANZ: Frank McGowan
- Japan: Masa Ueda
- India : Amit Kaul
- Taiwan and ASEAN: Teo Wan Ping
- Korea: Sang Hoon Kim
- Others: Dirk de Vos

</details>


<details>
<summary markdown="span">EMEA Select</summary>
- France: Tristan Ouin
- DACH: Ilaria Pazienza
- Poland: Ilaria Pazienza
- Italy: Ilaria Pazienza
- Cze: Ilaria Pazienza
- UK: Matthew Coughlan
- Nordics: Matthew Coughlan
- Benelux: Matthew Coughlan

</details>


<details>
<summary markdown="span">EMEA Open</summary>
- Middle East: Camille Dios
- Africa: Camille Dios
- Southern Europe: Camille Dios
- DACH: Bastian van der Stel
- Russia: Jags Bewas 
- Northern Europe: Jags Bewas 
- UKI: Jags Bewas
- Nordics: Jags Bewas
- Benelux: Jags Bewas

</details>


<details>
<summary markdown="span">EMEA Open</summary>
- Middle East: Camille Dios
- Africa: Camille Dios
- Southern Europe: Camille Dios
- DACH: Bastian van der Stel
- Russia: Jags Bewas 
- Northern Europe: Jags Bewas 
- UKI: Jags Bewas
- Nordics: Jags Bewas
- Benelux: Jags Bewas

</details>


<details>
<summary markdown="span">US-PubSec</summary>
- Open: Maria del Pilar Mejia
- For all others, refer to columns B&C in the [CAM Mapping FY23](https://docs.google.com/spreadsheets/d/1okdK1HoqM-POt6GySRadeFp5AWf0rJY43GoScUq0EX0/edit#gid=974923348) spreadsheet.

</details>


#### Channel Solutions Architects
- AMER: Bart Zhang, Matt Genelin, Jeremy Chen (GSIs)
- APAC: Samer Akkoub
   - Japan: Tsukasa Komatsubara
- EMEA: Peter Bozso, Christoph Leygraf
- US-PubSec: Ana Desai, Pete Raumann


### Channel Manager Opportunity Ownership
(How to tell which Channel Manager is assigned to an Opportunity)

In the [CAM Mapping FY23](https://docs.google.com/spreadsheets/d/1okdK1HoqM-POt6GySRadeFp5AWf0rJY43GoScUq0EX0/edit#gid=974923348) spreadsheet (“Mapping Tables” tab), (Non-US PubSec or Alliance) Channel Managers are mapped first by the Opp Owner’s User Data using the field `[Deprecated] Owner Segment-Geo-Region-Area,` and then secondarily by the field `Stamped Account Sales Territory.` These fields are found in the 'Comp and Attainment Information` section of an opportunity.   

When determining which Channel Manager is mapped to an opportunity, First look up the `[Deprecated] Owner Segment-Geo-Region-Area` field. If there’s a Channel Manager listed, that is the Channel Manager for the opportunity. If, instead of a Channel Manager, it says “Look at Territory,” then refer to the `Stamped Account Sales Territory` column. That name will be the Channel Manager for the opportunity.    

US PubSec and Alliance Channel Managers are mapped according to the partner and are listed on the two left-hand sections of the same spreadsheet. If the partner account is a GSI, use the GSI lookup columns on the far right of the spreadsheet.


## Receiving Reseller Purchase Orders, Quote Requests, and Order Status Requests 
As of FY23Q4, there should not be any resellers sending quote requests, orders, or order status requests to the Partner Support email box. Partners should be sending these to the Sales person or CAM assigned to their opportunity (see the [Channel Ops handbook](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#opportunity-requirements-to-request-a-quote) if you need to provide a link to Sales). 

If the partner doesn't know who that is or who to send their request to, they can then email Partner Order Operations (partnerorderops@gitlab.com). 
The [Common Response Template](https://docs.google.com/spreadsheets/d/1iPJh3sP6p3fc_FfJPe7d5uzWThe6FJ5W_3arGFm2Dj4/edit#gid=0) has been updated with scripts for various scenarios, explained below with the complete process for each.   

### Reseller Quote Request, PO Submission, or Status Request that includes Partner Order Operations (Partner Support was just copied)
Reply using the response from the [Common Response Template](https://docs.google.com/spreadsheets/d/1iPJh3sP6p3fc_FfJPe7d5uzWThe6FJ5W_3arGFm2Dj4/edit#gid=0). No additional steps are necessary.   

### Reseller Quote Request, PO Submission, or Status Request that does _not_ include Partner Order Operations as an email recipient
1. Look up the opportunity and confirm who the Field Seller/ISR is.
2. Reply to the email and reattach any documents that came with the original email. 
3. Add the sales rep, ISR, CAM on the opportunity ([not necessarily the CAM who owns the account](https://about.gitlab.com/handbook/resellers/partner-help-desk/#channel-manager-opportunity-ownership)), and add partnerorderops@gitlab.com to the “cc” field.
4. **For PO Submissions Only** Attach the PO to the correct opportunity in SFDC.
5. Reply using the appropriate response from the [Common Response Template](https://docs.google.com/spreadsheets/d/1iPJh3sP6p3fc_FfJPe7d5uzWThe6FJ5W_3arGFm2Dj4/edit#gid=0). 

## NFRs
More information to come soon!

### Teaching a Partner to Reduce Seats in a SaaS NFR
1. Let the partner know how many seats are in use, and copy the screenshot from support if it was provided. 
2. Link the [NFR Policy](NFR policy)
3. Add additional links for assistance:
   - [Determining Seat Usage](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined)
   - [See a complete view of usage](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#view-seat-usage)
4. Let them know they can confirm or remove users on the "Seats Quote Tab."
5. Once the partner replies that they have decreased their seats, update the support ticket for the NFR. 

## Invoices
More information to come soon!

### Partner Invoices
Partner invoices can be found on the Partner Account Record in SFDC under `Invoices.` It helps to either have the date of the transaction and/or when the related opportunity was Closed-Won in the system. 

